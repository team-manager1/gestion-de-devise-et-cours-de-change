import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import axios from 'axios';
import './App.css';
import { Onglet } from './home';

const ChangeGraph = ({ startDate, endDate }) => {
  const [exchangeRateHistory, setExchangeRateHistory] = useState([]);

  useEffect(() => {
    const fetchExchangeRateHistory = async () => {
      const currencies = ['EUR', 'USD', 'GBP', 'CHF', 'JPY', 'CAD', 'MUR'];
      const baseCurrency = 'MGA';

      try {
        const exchangeRates = [];

        for (const currency of currencies) {
          const response = await axios.get(
            `https://api.exchangerate-api.com/v4/latest/${baseCurrency}`
          );

          const rates = response.data.rates;

          if (rates) {
            const currencyRate = rates[currency];
            exchangeRates.push({
              currency,
              rate: currencyRate,
            });
          } else {
            console.error(`No exchange rate data available for ${currency}.`);
          }
        }

        setExchangeRateHistory(exchangeRates);
      } catch (error) {
        console.error(error);
        setExchangeRateHistory([]);
      }
    };

    fetchExchangeRateHistory();
  }, []);

  const generateChartData = (currency) => {
    const chartData = {
      labels: [],
      datasets: [],
    };

    const currencyData = exchangeRateHistory.find((data) => data.currency === currency);

    if (currencyData) {
      const { currency, rate } = currencyData;
      chartData.labels.push(currency);
      chartData.datasets.push({
        label: `Taux de change ${currency}-MGA`,
        data: [rate],
        fill: false,
        borderColor: getLabelColor(currency),
        pointBorderColor: getLabelColor(currency),
        pointBackgroundColor: `rgba(75, 192, 192, 0.2)`,
        tension: 0.1,
      });
    }

    return chartData;
  };

  const getLabelColor = (currency) => {
    switch (currency) {
      case 'EUR':
        return 'rgb(75, 192, 192)';
      case 'USD':
        return 'rgb(255, 99, 132)';
      case 'GBP':
        return 'rgb(54, 162, 235)';
      case 'CHF':
        return 'rgb(255, 205, 86)';
      case 'JPY':
        return 'rgb(153, 102, 255)';
      case 'CAD':
        return 'rgb(255, 159, 64)';
      case 'MUR':
        return 'rgb(201, 203, 207)';
      default:
        return 'rgb(0, 0, 0)';
    }
  };

  return (
    <div className="container-fluid text-center">
      <Onglet />
      <div className="row" style={{justifyContent: 'center'}}>
        {exchangeRateHistory && exchangeRateHistory.length > 0 ? (
          exchangeRateHistory.map((currencyData, index) => (
            <div key={index} className="col-12 col-md-6 col-lg-4 mb-4">
              <div className="card">
                <div className="card-body">
                  <h3 className="card-title">{`Taux de change ${currencyData.currency}-MGA`}</h3>
                  <Line
                    data={generateChartData(currencyData.currency)}
                    options={{ responsive: true }}
                  />
                </div>
              </div>
            </div>
          ))
        ) : (
          <p>Aucune donnée de taux de change disponible.</p>
        )}
      </div>
    </div>
  );
};

export default ChangeGraph;
