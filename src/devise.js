import { useEffect, useState } from 'react';
import Axios from 'axios';
import Dropdown from 'react-dropdown';
import { HiSwitchHorizontal } from 'react-icons/hi';
import 'react-dropdown/style.css';
import './App.css';
import { Onglet } from './home';
import Flag from 'react-world-flags';
import { Container, Row, Col } from 'react-bootstrap';

function Devise() {
  
  const [info, setInfo] = useState([]);
  const [input, setInput] = useState(0);
  const [from, setFrom] = useState('usd');
  const [to, setTo] = useState('inr');
  const [options, setOptions] = useState([]);
  const [output, setOutput] = useState(0);
  const [error, setError] = useState('');

  
  useEffect(() => {
    Axios.get(
      `https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/${from}.json`
    ).then((res) => {
      setInfo(res.data[from]);
    });
  }, [from]);

  
  useEffect(() => {
    setOptions(Object.keys(info));
    convert();
  }, [info, input]);


  function convert() {
    if (isNaN(input)) {
      setError("S'il vous plaît entrez un nombre valide.");
      setOutput(0);
    } else {
      setError('');
      var rate = info[to];
      setOutput(input * rate);
    }
  }

  
  function flip() {
    var temp = from;
    setFrom(to);
    setTo(temp);
  }

  function renderOption(option) {
    const isDefaultFromCurrency = option.value === from;
    const isDefaultToCurrency = option.value === to;

    return (
      <div className={isDefaultFromCurrency || isDefaultToCurrency ? 'default-currency' : ''}>
        <Flag code={option.value.substring(0, 2)} height="16" />
        <span>{option.label}</span>
      </div>
    );
  }

  return (
    <div className="App">
     
      
      <div className="container">
        <div className="left" style={{width:'250px'}}>
          <h3>Amount</h3>
          <input
            type="text"
            className="form-control"
            placeholder="Enter the amount"
            onChange={(e) => setInput(e.target.value)}
          />
          {error && <p className="error-message">{error}</p>}
        </div>
          <Container className="text-uppercase" >
            <Row>
              <Col>
              <h3 style={{ textTransform: 'capitalize' }}>From</h3>
              <div className="currency-container">
            
              <Dropdown
              
              options={options.map((option) => ({
                value: option,
                label: (
                  <div >
                    <Flag code={option.substring(0, 2)} height="16" />
                    <span className="text-uppercase">{option}</span>
                  </div>
                ),
              }))}
              onChange={(e) => {
                setFrom(e.value);
              }}
              value={from}
              placeholder="From"
              renderOption={renderOption} 
              className="form-control"
              />
              </div>
              </Col>
              <Col>
                <div className="switch" style={{
                textAlign:'center',
                marginTop:'50px'
                }} >
                <HiSwitchHorizontal size="30px" onClick={flip} />
                </div>      
              </Col>
              <Col>
          <h3 style={{ textTransform: 'capitalize' }}>To</h3>
          <div className="currency-container">
            
            <Dropdown 
              options={options.map((option) => ({
                value: option,
                label: (
                  <div>
                    <Flag code={option.substring(0, 2)} height="16" />
                    <span className="text-uppercase">{option}</span>
                  </div>
                ),
              }))}
              onChange={(e) => {
                setTo(e.value);
              }}
              value={to}
              placeholder="To"
              renderOption={renderOption} 
              className="form-control"
            />
            </div> 
              </Col>
            </Row>
          </Container>
      </div>
      <div className="result">
        <p></p>
        <button className="btn text-white" style={{background:'hwb(115 0% 50% / 0.76)'}} onClick={convert}>
          Convert
        </button>
        <h2>Converted Amount:</h2>
        <p className="text-uppercase">
          {input + ' ' + from + ' = ' + output.toFixed(5) + ' ' + to}
        </p>
      </div>
    </div>
  );
}


export default Devise;