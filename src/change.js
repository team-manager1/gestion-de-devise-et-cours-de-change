import React, { useState, useEffect } from 'react';
import { Onglet } from './home';
import Table from 'react-bootstrap/Table';
import axios from 'axios';

function getMonthName(month) {
  const months = [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre'
  ];
  return months[month];
}

function TableHeader() {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth();
  const monthName = getMonthName(month);
  const formattedDate = `${date.getDate()} ${monthName} ${year}`;

  return (
    <thead>
      <tr>
        <th colSpan="7">cours de référence du {formattedDate}</th>
      </tr>
     
    </thead>
  );
}

function Change() {
  const [exchangeRates, setExchangeRates] = useState(null);
  const currentDate = new Date();

  useEffect(() => {
    const fetchExchangeRates = async () => {
      try {
        const response = await axios.get('https://api.exchangerate-api.com/v4/latest/MGA');
        setExchangeRates(response.data.rates);
      } catch (error) {
        console.error(error);
      }
    };

    fetchExchangeRates();
  }, []);

  return (
    <>
      <Onglet />
      <Table striped style={{width:'900px',margin:'auto'}} >
        <TableHeader />
        <tbody >
          {exchangeRates && (
            <>
            <tr className='bg-warning'>
                <td colSpan={3}>DEVISE </td>
                <td >en MGA</td>
              </tr>
              <tr>
                <td colSpan={3}>EURO (EUR) &#x1F1EA;&#x1F1FA;</td>
                <td >{1 / exchangeRates['EUR']}</td>
              </tr>
              <tr>
                <td colSpan={3}>Dollar US (USD) &#x1F1FA;&#x1F1F8;</td>
                <td> {1/ exchangeRates['USD']}</td>
              </tr>
              <tr>
                <td colSpan={3}>Livre Britannique (GBP) &#x1F1EC;&#x1F1E7;</td>
                <td> {1/ exchangeRates['GBP']}</td>
              </tr>
              <tr>
                <td colSpan={3}>Franc Suisse (CHF) &#x1F1E8;&#x1F1ED;</td>
                <td> {1/ exchangeRates['CHF']}</td>
              </tr>
              <tr>
                <td colSpan={3}>Yen Japonais (JPY) &#x1F1EF;&#x1F1F5;</td>
                <td> {1/ exchangeRates['JPY']}</td>
              </tr>
              <tr>
                <td colSpan={3}>Dollar Canadien (CAD) &#x1F1E8;&#x1F1E6;</td>
                <td> {1/ exchangeRates['CAD']}</td>
              </tr>
              <tr>
                <td colSpan={3}>Roupie Mauricienne (MUR) &#x1F1F2;&#x1F1FA;</td>
                <td> {1/ exchangeRates['MUR']}</td>
              </tr>
            </>
          )}
        </tbody>
      </Table>

      <footer className="bg-warning text-white py-4" 
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
        }}>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <h5>La Banque</h5>
              <ul className="list-unstyled">
                <li>Nous connaître</li>
                <li>Communication financière</li>
                <li>Actualités</li>
                <li>Recrutement</li>
                <li>Contactez-nous</li>
              </ul>
            </div>
            <div className="col-md-4">
              <h5>Notre offre particuliers</h5>
              <ul className="list-unstyled">
                <li>Comptes &amp; Services</li>
                <li>Prêts</li>
                <li>Epargne et placements</li>
                <li>Assurances</li>
              </ul>
            </div>
            <div className="col-md-4">
              <h5>Autres offres BANK </h5>
              <ul className="list-unstyled">
                <li>Grandes Entreprises</li>
                <li>PME</li>
              </ul>
            </div>
          </div>
          
        </div>
      </footer>
    </>
  );
}

export default Change;
