import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter,Routes,Route} from 'react-router-dom';
import Home from "./home";
import Devise from "./devise";
import Change from './change';
import ChangeGraph from "./chart";



function App(){
  return(
    <div className='App'>
      <BrowserRouter>
        <Routes>
        <Route path='/' element={<Home/>}></Route>
        <Route path='/devise' element={<Devise/>}></Route>
        <Route path='/chart' element={<ChangeGraph/>}></Route>
        <Route path='/change' element={<Change/>}></Route>
        </Routes>
      </BrowserRouter>

    </div>
  )
}

export default App;


