import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Nav from 'react-bootstrap/Nav';
import './App.css';
import React, { useEffect } from 'react';
import { Container,Row,Col } from 'react-bootstrap';
import { useState } from 'react';
import { Toast } from 'react-bootstrap';
import axios from "axios";
import Devise from "./devise";

function Dismissible(){
  const [showA, setShowA] = useState(false);
  

  const toggleShowA = () => setShowA(!showA);
  
  return(
          <Row>
          <Col md={6} className="mb-2">
          <Button variant="outline-secondary" onClick={toggleShowA} className="mb-2" style={{color:'white'}}>
          En savoir plus
          </Button>
          <Toast show={showA} onClose={toggleShowA}>

          <Toast.Body className='bg-warning text-black'>
          Notre objectif est de rendre votre experience bancaire aussi simple et pratique que possible. Avec notre interface conviviale et securisee, vous pouvez acceder a vos comptes a tout moment, ou que vous soyez. Gerez vos depenses, effectuez des virements, consultez vos releves et bien plus encore, en quelques clics seulement.
          </Toast.Body>
          </Toast>
          </Col>

      </Row>

      

  )
}

export function Onglet(){

  return(
    <Nav  className='justify-content-end navbar'  defaultActiveKey="/">
    <Nav.Item>
      <Nav.Link href="/" className='text-black'  
      style={{
        borderBottom:'1px solid black',
        margin:'10px',
        padding:'10px 0'
      }}>Accueil</Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link href="/chart" eventKey="link-1"className='text-black' 
       style={{
        borderBottom:'1px solid black',
        margin:'10px',
        padding:'10px 0'
      }}>graphe</Nav.Link>
    </Nav.Item>
    <Nav.Item>
    <Example/>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link href="/change" eventKey="link-3" className='text-black'
       style={{
        borderBottom:'1px solid black',
        margin:'10px',
        padding:'10px 0'
      }}>cours de change </Nav.Link>
    </Nav.Item>
  </Nav>
  )
}

function Example() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
    
      <Nav.Link href="" onClick={handleShow} eventKey="link-2" className='text-black'
       style={{
        borderBottom:'1px solid black',
        margin:'10px',
        padding:'10px 0'
      }}>devise</Nav.Link>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Conversion de Devise</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Devise/>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
        </Modal.Footer>
      </Modal>
    </>
  );
}



function Home() {
    useEffect(()=>{
    axios.get('https://localhost:3001/')
    .then(res=> console.log(res))
    .then(err=> console.log(err))
  },[])
  
  return (
    <>
    <Onglet/>
    <div className="w-100">
        <img src="/image/moneys.jpeg" alt="Moneys" 
        style={{
           width:'100%',
           height: '100vh'
          }} />
        <div className='overlay'>
        <Container>
        <Row>
              <Col>
              <div className='text-wrapper'>
              <h1>Bienvenu sur le site de notre Banque!</h1>
              <h3>Nous sommes ravis de vous accueillir ici ,où vous pouvez accéder à tous nos services bancaire en ligne.</h3>
              <Dismissible/>
              </div>
              </Col>
          </Row>
        </Container>    
        </div>
    </div>
    </>
  );
}

export default Home;